//
// Created by stdd0 on 14.04.2020.
//

#ifndef WEAPONS_APP_FIREARM_H
#define WEAPONS_APP_FIREARM_H

#include <iostream>
#include <string>
#include "BaseWeapon.h"

class Firearm : public BaseWeapon {
protected:
    int distance;
public:
    Firearm(int dist, const string &name, const string &producer, int produceDate, bool isSouvenir) :
            BaseWeapon(name, producer, produceDate, isSouvenir) {
        distance = dist;
    }
    Firearm() = default;

    int GetDistance() { return distance; }

    void SetDistance(int dist) {
        distance = dist;
    }
    virtual double CalcPower();
    void Print();
    void Input();
};


#endif //WEAPONS_APP_FIREARM_H
