//
// Created by stdd0 on 03.06.2020.
//

#include "Coldsteel.h"
#include <string>
#include "Cutlass.h"

using namespace std;

void Cutlass::Print() {
    Coldsteel::Print();
    cout << "Force:" << GetForce() << endl;
    cout << "Application:" << GetApplication() << endl;
}
void Cutlass::Input() {
    Coldsteel::Input();
    int frc;
    string appl;
    cout << "Enter force:";
    cin >> frc;
    SetForce(frc);
    cout << endl;
    cout << "Enter application:";
    cin >> appl;
    SetApplication(appl);
}