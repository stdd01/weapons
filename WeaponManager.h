//
// Created by stdd0 on 01.06.2020.
//

#ifndef WEAPONSPROJECT_WEAPONMANAGER_H
#define WEAPONSPROJECT_WEAPONMANAGER_H

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include "BaseWeapon.h"

using namespace std;

class WeaponManager {
public:
    vector<BaseWeapon *> userWeapon;

    WeaponManager() = default;

    ~WeaponManager() {
        for (auto item : userWeapon) {
            delete item;
        }
    }

    void PrintMenu() {
        cout << "\n Choose kind of weapon";
        cout << "\n-----------------------------------------------------";
        cout << "\n 0 - Gun     1 - Automat     2 - Sword    3 - Cutlass";
        cout << "\n-----------------------------------------------------";
    };

    void PrintSortMenu() {
        cout << "\n 0 - Sort by produce date     1 - Sort by name \n";
    };

    void AddWeapon(BaseWeapon *weapon) {
        userWeapon.push_back(weapon);
    };

    void ListWeapon() {
        if (userWeapon.empty()) {
            cout << "\n The list is empty." << endl;
        } else {
            for (auto item : userWeapon) {
                item->Print();
                cout << endl;
            }
        }
    };

    void SearchWeapon(const string &search) {
        int count = 0;
        for (auto item : userWeapon) {
            size_t posName = item->GetName().find(search);
            size_t posProducer = item->GetProducer().find(search);

            if (posName != std::string::npos || posProducer != std::string::npos) {
                count++;
                item->Print();
                cout << "\n";
            }
        }

        if (!count) {
            cout << "\nSubstring not found." << endl;
        }
    };

    void SortWeapon(int &parameter) {
        switch (parameter) {
            case 0: {
                sort(userWeapon.begin(), userWeapon.end(), [](BaseWeapon *weapon1, BaseWeapon *weapon2) {
                    return weapon1->GetProduceDate() < weapon2->GetProduceDate();
                });
                break;
            }
            case 1: {
                sort(userWeapon.begin(), userWeapon.end(), [](BaseWeapon *weapon1, BaseWeapon *weapon2) {
                    return weapon1->GetName() < weapon2->GetName();
                });
                break;
            }
        }
    };
};


#endif //WEAPONSPROJECT_WEAPONMANAGER_H
