//
// Created by stdd0 on 14.04.2020.
//

#ifndef WEAPONSPROJECT_SWORD_H
#define WEAPONSPROJECT_SWORD_H

#include <string>
#include "Coldsteel.h"

class Sword : public Coldsteel{
protected:
    int length;
public:
    Sword(int lng1, int bladeLength, const string &material, const string &name, const string &producer, int produceDate, bool isSouvenir) :
            Coldsteel(bladeLength, material, name, producer, produceDate, isSouvenir) {
        length = lng1;
    }

    Sword() = default;
    ~Sword() = default;

    void SetLength(int lng1) {
        length = lng1;
    }
    int GetLength() { return length; }

    void Print();
    void Input();
};


#endif //WEAPONSPROJECT_SWORD_H
