#include <iostream>
#include <vector>
#include <windows.h>
#include "Gun.h"
#include "Automat.h"
#include "Sword.h"
#include "Cutlass.h"
#include "WeaponManager.h"

using namespace std;

void setColor(HANDLE hStdOut) {
    SetConsoleTextAttribute(hStdOut, BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_INTENSITY |
    FOREGROUND_RED | FOREGROUND_GREEN |FOREGROUND_BLUE );
}
void addChooseWeaponBlock(WeaponManager *manager) {

    manager->PrintMenu();

    int choice;
    cout << "\n Choose weapon to add: ";
    cin >> choice;
    switch (choice) {
        case 0: {
            Gun *gun = new Gun;
            gun->Input();
            manager->AddWeapon(gun);
            cout << endl;
            break;
        }
        case 1: {
            Automat *automat = new Automat;
            automat->Input();
            manager->AddWeapon(automat);
            cout << endl;
            break;
        }
        case 2: {
            Sword *sword = new Sword;
            sword->Input();
            manager->AddWeapon(sword);
            cout << endl;
            break;
        }
        case 3: {
            Cutlass *cutlass = new Cutlass;
            cutlass->Input();
            manager->AddWeapon(cutlass);
            cout << endl;
            break;
        }
        case 4: {
            break;
        }
    }

}

int main() {
    HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);

    WeaponManager *manager = new WeaponManager;

    while (true) {
        setColor(hStdOut);
        cout << "\n 0 - Add  1 - List  2 - Search  3 - Sort 4 - Exit ";

        SetConsoleTextAttribute(hStdOut, 12);

        int main_choice;
        SetConsoleTextAttribute(hStdOut, 10);
        cout << "\n Choose action: ";
        cin >> main_choice;

        if (main_choice == 4) {
            SetConsoleTextAttribute(hStdOut, BACKGROUND_RED|
                                             FOREGROUND_RED | FOREGROUND_GREEN |FOREGROUND_BLUE |  FOREGROUND_INTENSITY);
            cout << "Exit!";
            delete manager;
            return 0;
        }

        if (main_choice == 3) {
            int parameter;
            SetConsoleTextAttribute(hStdOut, 11);
            cout << "\n \nEnter parameter for sort: " << endl;
            manager->PrintSortMenu();
            cin >> parameter;
            manager->SortWeapon(parameter);
            manager->ListWeapon();
        }

        if (main_choice == 2) {
            string search;
            SetConsoleTextAttribute(hStdOut, 14);
            cout << "\n \nEnter substring for search: " << endl;
            cin >> search;
            manager->SearchWeapon(search);
        }

        if (main_choice == 1) {
            SetConsoleTextAttribute(hStdOut, 10);
            cout << "\n List:" << endl;
            SetConsoleTextAttribute(hStdOut, 4);
            manager->ListWeapon();
        }

        if (main_choice == 0) {
            SetConsoleTextAttribute(hStdOut, 11);
                addChooseWeaponBlock(manager);
            }
        }

}


