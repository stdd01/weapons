//
// Created by stdd0 on 03.06.2020.
//

#include <iostream>
#include "BaseWeapon.h"
#include "Firearm.h"

using namespace std;

double Firearm::CalcPower() {
    return 1.75 * GetDistance();
}
void Firearm::Print() {
    BaseWeapon::Print();
    cout << "Distance:" << GetDistance() << endl;
}
void Firearm::Input() {
    BaseWeapon::Input();
    int dist;
    cout << "Enter distance:";
    cin >> dist;
    SetDistance(dist);
}