//
// Created by stdd0 on 03.06.2020.
//

#include <iostream>
#include "Firearm.h"
#include "Gun.h"

using namespace std;

double Gun::CalcPower() {
    return (GetPower() * GetCaliber() * 1.25) / 9.8 / 1000;
}

void Gun::Print() {
    Firearm::Print();
    cout << "Caliber:" << GetCaliber() << endl
         << "Power:" << GetPower() << endl
         << "CalcPower:" << CalcPower() << endl;
}

void Gun::Input() {
    Firearm::Input();
    int pwr;
    int calib;
    cout << "Enter caliber:";
    cin >> calib;
    SetCaliber(calib);
    cout << "Enter power:";
    cin >> pwr;
    SetPower(pwr);
}