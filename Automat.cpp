//
// Created by stdd0 on 03.06.2020.
//

#include <iostream>
#include "Firearm.h"
#include "Automat.h"

using namespace std;

double Automat::CalcPower() {
    return (GetLength() * 1275) / 9.8 / 3;
}

void Automat::Print() {
    Firearm::Print();
    cout << "Length:" << GetLength() << endl
         << "CalcPower:" << CalcPower() << endl;
}

void Automat::Input() {
    Firearm::Input();
    int lng;
    cout << "Enter length:";
    cin >> lng;
    SetLength(lng);
}