//
// Created by stdd0 on 14.04.2020.
//

#ifndef WEAPONS_APP_BASEWEAPON_H
#define WEAPONS_APP_BASEWEAPON_H
#include <iostream>
#include <string>
#include <ctime>

using namespace std;

class BaseWeapon {
protected:
    string name;
    string producer;
    int produceDate;
    bool isSouvenir;

public:
    BaseWeapon(const string &name, const string &producer, int produceDate, bool isSouvenir) :
            name(name),
            producer(producer),
            produceDate(produceDate),
            isSouvenir(isSouvenir) {};

    BaseWeapon() = default;

    string GetName() { return name; }
    string GetProducer() { return producer; }
    int GetProduceDate() { return produceDate; }
    bool GetIsSouvenir() { return isSouvenir;  }

    void SetName(string nm) {
        name = nm;
    }
    void SetProducer(string prod) {
        producer = prod;
    }
    void SetProduceDate(int prodDate) {
        produceDate = prodDate;
    }
    void SetIsSouvenir(bool isSouv) {
        isSouvenir = isSouv;
    }

    virtual void Print();
    virtual void Input();
};

#endif //WEAPONS_APP_BASEWEAPON_H
