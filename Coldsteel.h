//
// Created by stdd0 on 14.04.2020.
//

#ifndef WEAPONSPROJECT_COLDSTEEL_H
#define WEAPONSPROJECT_COLDSTEEL_H

#include <iostream>
#include <string>
#include "BaseWeapon.h"

class Coldsteel : public BaseWeapon {
protected:
    int bladeLength;
    string material;
public:
    Coldsteel(int blade1, const string &mtr, const string &name, const string &producer, int produceDate,
              bool isSouvenir) :
            BaseWeapon(name, producer, produceDate, isSouvenir) {
        bladeLength = blade1;
        material = mtr;
    }

    Coldsteel() = default;

    int GetBladeLength() { return bladeLength; }

    string GetMaterial() { return material; }

    void SetBladeLength(int blade1) {
        bladeLength = blade1;
    }

    void SetMaterial(string mtr) {
        material = mtr;
    }

    void Print();

    void Input();

};


#endif //WEAPONSPROJECT_COLDSTEEL_H
