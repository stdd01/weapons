//
// Created by stdd0 on 14.04.2020.
//

#ifndef WEAPONSPROJECT_AUTOMAT_H
#define WEAPONSPROJECT_AUTOMAT_H

#include <iostream>
#include <string>
#include "Firearm.h"

class Automat : public Firearm {
protected:
    int length;
public:
    Automat(int lng, int distance, const string &name, const string &producer, int produceDate, bool isSouvenir) :
            Firearm(distance, name, producer, produceDate, isSouvenir) {
        length = lng;
    }
    Automat() = default;
    ~Automat() = default;
    int GetLength() { return length; }
    void SetLength(int lng) {
        length = lng;
    }
    double CalcPower();
    void Print();
    void Input();
};


#endif //WEAPONSPROJECT_AUTOMAT_H
