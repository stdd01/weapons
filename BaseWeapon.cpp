//
// Created by stdd0 on 03.06.2020.
//

#include <iostream>
#include <string>
#include "BaseWeapon.h"

using namespace std;

void BaseWeapon::Print() {
    cout << "Weapon Type: " << typeid(*this).name() << endl
         << "Name:" << GetName() << endl
         << "Producer:" << GetProducer() << endl
         << "Produce Date:" << GetProduceDate() << endl
         << "Souvenir:" << GetIsSouvenir() << endl;
}
void BaseWeapon::Input() {
    string nm, prod;
    int prodDate;
    bool isSouv;

    cout << "\nEnter name:";
    cin >> nm;
    SetName(nm);
    cout << "Enter producer:";
    cin >> prod;
    SetProducer(prod);
    cout << "Enter produce Date:";
    cin >> prodDate;
    SetProduceDate(prodDate);
    cout << "Is souvenir?";
    cin >> isSouv;
    SetIsSouvenir(isSouv);
}