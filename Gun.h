//
// Created by stdd0 on 14.04.2020.
//

#ifndef WEAPONSPROJECT_GUN_H
#define WEAPONSPROJECT_GUN_H

#include <iostream>
#include "Firearm.h"
#include <string>

class Gun : public Firearm {
protected:
    int caliber;
    int power;
public:
    Gun(int calib, int pwr, int distance, const string &name, const string &producer, int produceDate, bool isSouvenir) :
            Firearm(distance, name, producer, produceDate, isSouvenir) {
        caliber = calib;
        power = pwr;
    }

    Gun() = default;
    ~Gun() = default;

    int GetCaliber() { return caliber; }
    int GetPower() { return power; }
    void SetCaliber(int calib) {
        caliber = calib;
    }
    void SetPower(int pwr) {
        power = pwr;
    }

    double CalcPower();
    void Print();
    void Input();
};


#endif //WEAPONSPROJECT_GUN_H
