//
// Created by stdd0 on 03.06.2020.
//
#include <iostream>
#include "Coldsteel.h"
#include "Sword.h"

using namespace std;

void Sword::Print() {
    Coldsteel::Print();
    cout << "Length:" << GetLength() << endl;
}
void Sword::Input() {
    Coldsteel::Input();
    int lng1;
    cout << "Enter length:";
    cin >> lng1;
    SetLength(lng1);
}