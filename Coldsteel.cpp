//
// Created by stdd0 on 03.06.2020.
//

#include <iostream>
#include <string>
#include "BaseWeapon.h"
#include "Coldsteel.h"

using namespace std;

void Coldsteel::Print() {
    BaseWeapon::Print();
    cout << "BladeLength:" << GetBladeLength() << endl
         << "Material:" << GetMaterial() << endl;
}
void Coldsteel::Input() {
    BaseWeapon::Input();
    int blade1;
    string mtr;
    cout << "Enter length of blade:";
    cin >> blade1;
    SetBladeLength(blade1);
    cout << "Enter material:";
    cin >> mtr;
    SetMaterial(mtr);
}
