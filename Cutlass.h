//
// Created by stdd0 on 14.04.2020.
//

#ifndef WEAPONSPROJECT_CUTLASS_H
#define WEAPONSPROJECT_CUTLASS_H

#include <string>
#include "Coldsteel.h"

class Cutlass : public Coldsteel {
protected:
    int force;
    string application;
public:
    Cutlass(int frc, int bladeLength, const string &appl, const string &material, const string &name,
            const string &producer, int produceDate, bool isSouvenir) :
            Coldsteel(bladeLength, material, name, producer, produceDate, isSouvenir) {
        force = frc;
        application = appl;
    }

    Cutlass() = default;
    ~Cutlass() = default;

    void SetForce(int frc) {
        force = frc;
    }
    void SetApplication(string appl) {
        application = appl;
    }
    int GetForce() { return force; }
    string GetApplication() { return application; }
    void Print();
    void Input();
};


#endif //WEAPONSPROJECT_CUTLASS_H
